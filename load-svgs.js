$(document).ready(function(){
    $(".cloud1, .cloud2").load("//glen-misc.gitlab.io/starchild/cloud_large.html");
    
    $(".cloud3").load("//glen-misc.gitlab.io/starchild/cloud_alt.html");
    
    $(".bgcloud1, .bgcloud3, .bgcloud4").load("//glen-misc.gitlab.io/starchild/cloud_bg1.html");
    
    $(".bgcloud2").load("//glen-misc.gitlab.io/starchild/cloud_bg2.html");
    
    $(".glen-svg").load("//glen-misc.gitlab.io/starchild/glencrd.html");
}); //end ready
