$(document).ready(function(){
// i wrote this shuffle script myself i feel like a god
// i know it's simple JS but alas
var songcount = $(".one-song-row:visible").length;
document.getElementById("count").innerHTML = songcount;
    
$(".shufflebutton").click(function(){
    var totalSongs = $(".one-song-row:visible").length;
    var getTrackno = Math.floor(Math.random() * totalSongs) + 1;
    var trackstring = "track" + getTrackno;
    var div_with_hash = "#" + trackstring;
    var selectedAudio = document.getElementById(trackstring);
    
    selectedAudio.currentTime = 0;
    selectedAudio.play();
    
    $(".one-song-row").removeClass("whenplaying");
    $(div_with_hash).parents(".one-song-row").addClass("whenplaying");

});// end shuffle-click

//----- play/pause each song on click

$(".one-song-row").click(function(){
    $(this).toggleClass("whenplaying");
    var plswork = $(this).find("audio").attr("id");
    
    var fuckingplayit = document.getElementById(plswork);
    
    if (fuckingplayit.paused) {
        fuckingplayit.currentTime = 0;
        fuckingplayit.play();
    } else {
        fuckingplayit.pause();
    }
    $(".one-song-row").not(this).removeClass("whenplaying");
});

});// ready end

$(window).load(function(){
    $(".dur").fadeIn();
});//end winload

document.addEventListener('play', function(e){
    var audios = document.getElementsByTagName('audio');
    for(var i = 0, len = audios.length; i < len;i++){
        if(audios[i] != e.target){
            audios[i].pause();
        }
    }
}, true);

// trigger click credit:
// stackoverflow.com/a/44825201/8144506
function fireClick(node){
    if(document.createEvent){
        var evt = document.createEvent('MouseEvents');
        evt.initEvent('click', true, false);
        node.dispatchEvent(evt);    
    } else if( document.createEventObject ) {
        node.fireEvent('onclick') ; 
    } else if (typeof node.onclick == 'function' ) {
        node.onclick(); 
    }
}

document.addEventListener('ended', function(e){
  let se = e.target.id;
  let rt = Number(se.replace(/[^\d\.]*/g,""));
  let rz = Math.round(rt+1);
  let tz = document.querySelector(".one-song-row.track" + rz);
  if(tz){
    fireClick(tz)
  } else {
    document.querySelector(".one-song-row." + se).classList.remove("whenplaying")
  }
}, true);
